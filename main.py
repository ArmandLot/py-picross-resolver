import tkinter as tk

# Set number of rows and columns
ROWS = 5
COLS = 5
canvas = None

# Create a grid of None to store the references to the tiles
tiles = [[None for _ in range(COLS)] for _ in range(ROWS)]

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_ui()

    def create_ui(self):
        self.resolve_grid = tk.Button(self)
        self.resolve_grid["text"] = "Resolve!"
        self.resolve_grid["command"] = resolve_grid
        self.resolve_grid.pack(side="bottom")

def resolve_grid():
    count = 0
    for col in tiles:
        for row in col:
            if row != None:
                count = count + 1
    print("Found:",count,"non-empty cell(s).")


def callback(event):
    # Get rectangle diameters
    col_width = canvas.winfo_width()/COLS
    row_height = canvas.winfo_height()/ROWS
    # Calculate column and row number
    col = int(event.x//col_width)
    row = int(event.y//row_height)
    # If the tile is not filled, create a rectangle
    if not tiles[row][col]:
        tiles[row][col] = canvas.create_rectangle(col*col_width, row*row_height, (col+1)*col_width, (row+1)*row_height, fill="black")
    # If the tile is filled, delete the rectangle and clear the reference
    else:
        canvas.delete(tiles[row][col])
        tiles[row][col] = None

# Create the window, a canvas and the mouse click event binding
root = tk.Tk()
canvas = tk.Canvas(root, width=500, height=500, borderwidth=5, background='white')
canvas.pack()
canvas.bind("<Button-1>", callback)
app = Application(master=root)

app.mainloop()
